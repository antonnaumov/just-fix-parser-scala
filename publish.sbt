enablePlugins(AssemblyPlugin)

test            in assembly := {}
mainClass       in assembly := Some("org.bitbucket.antonnaumov.justfixparser.Launcher")
assemblyJarName in assembly := "parser.jar"