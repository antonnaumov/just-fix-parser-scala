package org.bitbucket.antonnaumov.justfixparser

import java.io.{InputStreamReader, StringReader}
import java.time.LocalDateTime

import org.bitbucket.antonnaumov.justfixparser.model.{Buy, Sell}
import org.bitbucket.antonnaumov.justfixparser.processor.AccumulateMessageProcessor

class ParserSpec extends BaseSpec {
  private val processor = new AccumulateMessageProcessor()
  private val parser = new Parser(processor)

  "Parser" should "parse and accept a message" in {
    val data = "8=FIX.4.4\u00019=142\u000135=W\u000134=0\u000149=justtech\u000152=20180206-21:43:36.000\u000156=user\u0001262=TEST\u000155=EURUSD\u0001268=2\u0001269=0\u0001270=1.31678\u0001271=100000.0\u0001269=1\u0001270=1.31667\u0001271=100000.0\u000110=057\u0001"
    parser.parse(new StringReader(data))
    processor.acceptedMessageCount should equal(1)
    processor.rejectedMessageCount should equal(0)
    processor.messages.headOption match {
      case Some(message) =>
        message.currencyPair should equal("EURUSD")
        message.timestamp should equal(LocalDateTime.of(2018, 2, 6, 21, 43, 36, 0))
        message.operations.size should equal(2)
        message.operations.find(_.operationType == Buy) match {
          case Some(op) =>
            op.operationType should equal(Buy)
            op.price should equal(BigDecimal(1.31678D))
            op.amount should equal(BigDecimal(100000.0D))
          case None =>
            fail("The message buy operation is not store in the type")
        }
        message.operations.find(_.operationType == Sell) match {
          case Some(op) =>
            op.operationType should equal(Sell)
            op.price should equal(BigDecimal(1.31667D))
            op.amount should equal(BigDecimal(100000.0D))
          case None =>
            fail("The message sell operation is not store in the type")
        }
      case None =>
        fail("The message is never rich message processor")
    }
  }

  it should "parse and reject a message with invalid check sum" in {
    val data = "8=FIX.4.4\u00019=142\u000135=W\u000134=0\u000149=justtech\u000152=20180206-21:43:36.000\u000156=user\u0001262=TEST\u000155=EURUSD\u0001268=2\u0001269=0\u0001270=1.31678\u0001271=100000.0\u0001269=1\u0001270=1.31667\u0001271=100000.0\u000110=055\u0001"
    parser.parse(new StringReader(data))
    processor.acceptedMessageCount should equal(0)
    processor.rejectedMessageCount should equal(1)
  }

  it should "parse and reject a message with invalid operation" in {
    val data = "8=FIX.4.4\u00019=142\u000135=W\u000134=0\u000149=justtech\u000152=20180206-21:43:36.000\u000156=user\u0001262=TEST\u000155=EURUSD\u0001268=2\u0001269=0\u0001270=1.31678\u0001271=100000.0\u000110=055\u0001"
    parser.parse(new StringReader(data))
    processor.acceptedMessageCount should equal(0)
    processor.rejectedMessageCount should equal(1)
  }

  it should "parse and reject a correct message doesn't describe buy-sell operations" in {
    val data = "8=FIX.4.1\u00019=61\u000135=A\u000134=1\u000152=20000426-12:05:06\u000198=0\u0001108=30\u000110=157\u0001"
    parser.parse(new StringReader(data))
    processor.acceptedMessageCount should equal(0)
    processor.rejectedMessageCount should equal(1)
  }

  it should "parse batch messages from file" in {
    val is = Thread.currentThread().getContextClassLoader().getResourceAsStream("example-fix-data.bin")
    parser.parse(new InputStreamReader(is))
    processor.acceptedMessageCount should equal(10)
    processor.rejectedMessageCount should equal(0)
  }

  override protected def beforeEach(): Unit = {
    processor.reset()
  }
}
