package org.bitbucket.antonnaumov.justfixparser

import org.scalatest.{BeforeAndAfterEach, FlatSpecLike, Matchers}

trait BaseSpec extends FlatSpecLike with Matchers with BeforeAndAfterEach
