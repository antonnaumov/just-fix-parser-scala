package org.bitbucket.antonnaumov.justfixparser.processor

import org.bitbucket.antonnaumov.justfixparser.MessageProcessor
import org.bitbucket.antonnaumov.justfixparser.model.Message

import scala.collection.mutable.ListBuffer

class AccumulateMessageProcessor extends MessageProcessor {
  private var rejectedCount: Int = 0
  private val messageList: ListBuffer[Message] = ListBuffer.empty

  override def acceptMessage(message: Message): Unit = {
    messageList += message
  }

  override def rejectMessage(reason: Exception): Unit = {
    rejectedCount += 1
  }

  def acceptedMessageCount: Int = messages.size

  def rejectedMessageCount: Int = rejectedCount

  def messages: List[Message] = messageList.toList

  def reset(): Unit = {
    rejectedCount = 0
    messageList.clear()
  }
}
