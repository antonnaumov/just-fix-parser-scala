package org.bitbucket.antonnaumov.justfixparser.processor

import java.time.format.DateTimeFormatter

import org.bitbucket.antonnaumov.justfixparser.model.{Buy, Message, OperationType, Sell}
import org.bitbucket.antonnaumov.justfixparser.MessageProcessor

class ConsoleOutputMessageProcessor extends MessageProcessor {
  import ConsoleOutputMessageProcessor._

  private var acceptedMessageCount = 0
  private var rejectedMessageCount = 0

  override def acceptMessage(message: Message): Unit = {
    val (buyPrice, buyAmount) = message.findOperationPriceAndAmount(Buy)
    val (sellPrice, sellAmount) = message.findOperationPriceAndAmount(Sell)
    acceptedMessageCount += 1
    System.out.format(
      RowTemplate,
      message.currencyPair,
      buyPrice.doubleValue: java.lang.Double,
      sellPrice.doubleValue: java.lang.Double,
      buyAmount.doubleValue: java.lang.Double,
      sellAmount.doubleValue: java.lang.Double,
      DateTimeFormat.format(message.timestamp))

  }

  override def rejectMessage(reason: Exception): Unit = {
    rejectedMessageCount +=1
  }

  def printHeader(): Unit = {
    System.out.format(SplitterTemplate)
    System.out.format(HeaderTemplate)
    System.out.format(SplitterTemplate)
  }

  def printFooter(): Unit = {
    System.out.format(SplitterTemplate)
    System.out.format(FooterTemplate, acceptedMessageCount: java.lang.Integer, rejectedMessageCount: java.lang.Integer)
    System.out.format(SplitterTemplate)
  }
}

object ConsoleOutputMessageProcessor {
  private val DateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS")
  private val SplitterTemplate =
    "+--------+-----------+------------+------------+-------------+-------------------------+%n"
  private val HeaderTemplate =
    "| Symbol | Buy Price | Sell Price | Buy Amount | Sell Amount |        Timestamp        |%n"
  private val RowTemplate =
    "| %6s |  %1.5f  |   %1.5f  | %(,.2f | %(,.2f  | %21s |%n"
  private val FooterTemplate =
    "| Total: | Rejected: %-4d, Accepted: %-4d                                              |%n"

  implicit class MessageExt(message: Message) {
    def findOperationPriceAndAmount(operationType: OperationType): (BigDecimal, BigDecimal) = {
      message.operations.find(_.operationType == Buy).fold(BigDecimal(0) -> BigDecimal(0))(op => op.price -> op.amount)
    }
  }
}
