package org.bitbucket.antonnaumov.justfixparser

import org.bitbucket.antonnaumov.justfixparser.model._

import scala.collection.mutable.ListBuffer
import scala.util.{Success, Try}

class MessageBuilder() {
  private val operations: ListBuffer[MessageEntryOperation] = ListBuffer.empty
  private val checkSumSource: ListBuffer[String] = ListBuffer.empty

  private var symbol: Option[MessageEntrySymbol] = None
  private var timestamp: Option[MessageEntryTimestamp] = None
  private var operationCount: Option[MessageEntryOperationCount] = None
  private var operationType: Option[MessageEntryOperationType] = None
  private var operationPrice: Option[MessageEntryOperationPrice] = None
  private var operationAmount: Option[MessageEntryOperationAmount] = None

  def withEntry[T <: MessageEntry[_]](entry: T): MessageBuilder = {
    entry match {
      case _: MessageEntryStart =>
        symbol = None
        timestamp = None
        operationCount = None
        cleanOperationEntries()
        operations.clear()
        checkSumSource.clear()
      case symbol: MessageEntrySymbol =>
        this.symbol = Some(symbol)
      case timestamp: MessageEntryTimestamp =>
        this.timestamp = Some(timestamp)
      case operationCount: MessageEntryOperationCount =>
        this.operationCount = Some(operationCount)
      case operationType: MessageEntryOperationType =>
        this.operationType = Some(operationType)
        composeOperation()
      case operationPrice: MessageEntryOperationPrice =>
        this.operationPrice = Some(operationPrice)
        composeOperation()
      case operationAmount: MessageEntryOperationAmount =>
        this.operationAmount = Some(operationAmount)
        composeOperation()
      case _ =>
    }
    checkSumSource += entry.entry
    this
  }

  def build(checkSum: MessageEntryCheckSum): Try[Message] = {
    for {
      _     <- validateOperationSize
      _     <- validateCheckSum(checkSum)
      pair  <- symbol.map(_.materialize).getOrElse(Try(throw InconsistentMessageStateException("currencyPair")))
      ts    <- timestamp.map(_.materialize).getOrElse(Try(throw InconsistentMessageStateException("timestamp")))
    } yield {
      val (success, failure) = operations.map(_.materialize).partition(_.isSuccess)
      failure.headOption.fold({
        Message(
          pair,
          ts,
          success.collect({case Success(o) => o}).toList
        )
      }) {er =>
        throw er.failed.get
      }
    }
  }

  private def validateOperationSize: Try[Int] = {
    operationCount.map(_.materialize.filter(_ == operations.size)).getOrElse(Try(throw InconsistentMessageStateException("operations")))
  }

  private def validateCheckSum(checkSum: MessageEntryCheckSum): Try[Unit] = {
    checkSum.materialize.map { expectedCheckSum =>
      val actualCheckSum = s"${checkSumSource.mkString("\u0001")}\u0001".getBytes().sum % 256
      if (expectedCheckSum != actualCheckSum) {
        throw InvalidMessageCheckSumException(actualCheckSum, expectedCheckSum)
      }
    }
  }

  private def composeOperation(): Unit = {
    if (operationType.nonEmpty && operationPrice.nonEmpty && operationAmount.nonEmpty) {
      operations += MessageEntryOperation(operationType.get, operationPrice.get, operationAmount.get)
      cleanOperationEntries()
    }
  }

  private def cleanOperationEntries(): Unit = {
    operationType = None
    operationPrice = None
    operationAmount = None
  }
}