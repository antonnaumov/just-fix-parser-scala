package org.bitbucket.antonnaumov.justfixparser

import java.io.{File, FileReader}

import org.bitbucket.antonnaumov.justfixparser.processor.ConsoleOutputMessageProcessor
import scopt.OptionParser

object Launcher extends App {
  sealed case class CmdOptions(source: File = new File("."))

  private val commandLineParser: OptionParser[CmdOptions] = {
    new OptionParser[CmdOptions]("MDF Message Parser") {
      head("Parse Market Data Full Refresh messages from the input file")

      arg[File]("<source>")
        .required()
        .action((file, c) => c.copy(source = file))
        .text("the file contains various Market Data Full Refresh messages")
    }
  }
  private val processor = new ConsoleOutputMessageProcessor()
  private val mdfParser = new Parser(processor)

  commandLineParser.parse(args, CmdOptions()) foreach { config =>
    processor.printHeader()
    mdfParser.parse(new FileReader(config.source))
    processor.printFooter()
  }

}
