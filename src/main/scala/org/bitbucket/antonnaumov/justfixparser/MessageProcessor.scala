package org.bitbucket.antonnaumov.justfixparser

import org.bitbucket.antonnaumov.justfixparser.model.Message

trait MessageProcessor {
  def acceptMessage(message: Message): Unit

  def rejectMessage(reason: Exception): Unit
}
