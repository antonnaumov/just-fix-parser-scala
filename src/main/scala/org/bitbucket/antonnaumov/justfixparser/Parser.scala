package org.bitbucket.antonnaumov.justfixparser

import java.util.Scanner
import java.util.regex.Pattern

import org.bitbucket.antonnaumov.justfixparser.model._

import scala.util.{Failure, Success, Try}

class Parser(processor: MessageProcessor) {
  import Parser._

  private val builder = new MessageBuilder()

  def parse(source: Readable): Unit = {
    val scanner = new Scanner(source).useDelimiter(MessageDelimiter)
    while(scanner.hasNext) {
      parseEntry(scanner.next()) match {
        case cs: MessageEntryCheckSum =>
          builder.build(cs) match {
            case Success(message) =>
              processor.acceptMessage(message)
            case Failure(e: Exception) =>
              processor.rejectMessage(e)
          }
        case entry =>
          builder.withEntry(entry)
      }
    }
  }

  private def parseEntry(entry: String): MessageEntry[_] = {
    entry2KeyValueTry(entry).get match {
      case (8, version) =>
        MessageEntryStart(entry, version)
      case (10, checkSum) =>
        MessageEntryCheckSum(entry, checkSum)
      case (52, timestamp) =>
        MessageEntryTimestamp(entry, timestamp)
      case (55, symbol) =>
        MessageEntrySymbol(entry, symbol)
      case (268, operationsCount) =>
        MessageEntryOperationCount(entry, operationsCount)
      case (269, operationType) =>
        MessageEntryOperationType(entry, operationType)
      case (270, operationPrice) =>
        MessageEntryOperationPrice(entry, operationPrice)
      case (271, operationAmount) =>
        MessageEntryOperationAmount(entry, operationAmount)
      case _ =>
        MessageEntryOther(entry)
    }
  }
}

object Parser {
  private val MessageDelimiter: Pattern = "\u0001".r.pattern
  private val KyeValueDelimiter: String = "="

  private def entry2KeyValueTry(entry: String): Try[(Int, String)] = {
    Try(entry.split(KyeValueDelimiter).toList match {
      case key :: value :: Nil => key.toInt -> value
      case _ => throw InconsistentMessageStateException(entry)
    }).recoverWith {
      case _: NumberFormatException => throw InconsistentMessageStateException(entry)
    }
  }
}
