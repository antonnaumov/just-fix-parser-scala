package org.bitbucket.antonnaumov.justfixparser

import java.time.LocalDateTime
import java.time.format.{DateTimeFormatter, DateTimeParseException}

import scala.util.Try

object model {
  sealed trait OperationType
  object OperationType {
    def apply(value: String): OperationType = {
      if (value == "0") Buy else Sell
    }
  }
  case object Buy extends OperationType
  case object Sell extends OperationType

  case class CurrencyOperation(operationType: OperationType, price: BigDecimal, amount: BigDecimal)
  case class Message(currencyPair: String, timestamp: LocalDateTime, operations: List[CurrencyOperation])

  sealed trait MessageEntry[T] {
    val entry: String
    val value: String

    def materialize: Try[T]
  }
  sealed trait OperationEntry
  case class MessageEntryStart(entry: String, value: String) extends MessageEntry[String] {
    override def materialize: Try[String] = Try(value)
  }
  case class MessageEntryCheckSum(entry: String, value: String) extends MessageEntry[Int] {
    override def materialize: Try[Int] = Try(value.toInt).recoverWith {
      case e: NumberFormatException => throw InconsistentMessageStateException("checkSum", Some(e))
    }
  }
  case class MessageEntryTimestamp(entry: String, value: String) extends MessageEntry[LocalDateTime] {
    private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd-HH:mm:ss.SSS")

    override def materialize: Try[LocalDateTime] = Try(LocalDateTime.parse(value, dateTimeFormatter)).recoverWith {
      case e: DateTimeParseException => throw InconsistentMessageStateException("timestamp", Some(e))
    }
  }
  case class MessageEntrySymbol(entry: String, value: String) extends MessageEntry[String] {
    override def materialize: Try[String] = Try(value)
  }
  case class MessageEntryOperationCount(entry: String, value: String) extends MessageEntry[Int] {
    override def materialize: Try[Int] = Try(value.toInt).recoverWith {
      case e: NumberFormatException => throw InconsistentMessageStateException("messageCount", Some(e))
    }
  }
  case class MessageEntryOperationType(entry: String, value: String) extends MessageEntry[OperationType] with OperationEntry {
    override def materialize: Try[OperationType] = Try(OperationType(value))
  }
  case class MessageEntryOperationPrice(entry: String, value: String) extends MessageEntry[BigDecimal] with OperationEntry {
    override def materialize: Try[BigDecimal] = Try(BigDecimal(value)).recoverWith {
      case e: NumberFormatException => throw InconsistentMessageStateException("operationPrice", Some(e))
    }
  }
  case class MessageEntryOperationAmount(entry: String, value: String) extends MessageEntry[BigDecimal] with OperationEntry {
    override def materialize: Try[BigDecimal] = Try(BigDecimal(value)).recoverWith {
      case e: NumberFormatException => throw InconsistentMessageStateException("operationAmount", Some(e))
    }
  }
  case class MessageEntryOperation(operationType: MessageEntryOperationType,
                                   price: MessageEntryOperationPrice,
                                   amount: MessageEntryOperationAmount) extends MessageEntry[CurrencyOperation] {
    override val entry: String = List(operationType.entry, price.entry, amount.entry).mkString("\u0001")

    override val value: String = ""

    override def materialize: Try[CurrencyOperation] = {
      for {
        op <- operationType.materialize
        p <- price.materialize
        a <- amount.materialize
      } yield {
        CurrencyOperation(op, p, a)
      }
    }
  }
  case class MessageEntryOther(entry: String) extends MessageEntry[Unit] {
    val value: String = ""

    override def materialize: Try[Unit] = Try{}
  }

  case class InconsistentMessageStateException(field: String, cause: Option[Exception] = None)
    extends Exception(s"The field value [" + field + "] is inconsistent", cause.orNull)
  case class InvalidMessageCheckSumException(actualCheckSum: Int, expectedCheckSum: Int)
    extends Exception(s"The message actual check sum [$actualCheckSum] is not equals with expected one [$expectedCheckSum]")
}
